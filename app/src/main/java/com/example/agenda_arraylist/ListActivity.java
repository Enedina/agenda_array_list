package com.example.agenda_arraylist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SearchView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity {

    TableLayout tbLists;
    ArrayList<Contacto>contactos;
    private ArrayList<Contacto> filther;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        tbLists=(TableLayout) findViewById(R.id.tbLista);
        Bundle bundleObject = getIntent().getExtras();
        contactos = (ArrayList<Contacto>) bundleObject.getSerializable("contactos");
        filther= contactos;
        Button btnNuevo = (Button) findViewById(R.id.btnNuevo);

        btnNuevo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(Activity.RESULT_CANCELED);
                finish();
            }
        });
        cargarContactos();
    }

    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.searchview,menu);
        MenuItem menuItem = menu.findItem(R.id.menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                buscar(s);
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void buscar(String s){
        ArrayList<Contacto> list = new ArrayList<>();
        for (int x=0;x<filther.size(); x++){
            if(filther.get(x).getNombre().contains(s))
                list.add(filther.get(x));
        }
        contactos = list;
        tbLists.removeAllViews();
        cargarContactos();
    }

    public void cargarContactos()
    {
        for (int x=0; x<contactos.size(); x++){

            Contacto c = contactos.get(x);
            TableRow nRow=new TableRow(ListActivity.this);
            Log.w("Error", c.getNombre());
            TextView nText = new TextView(ListActivity.this);
            nText.setTextColor((c.isFavorito())? Color.BLUE: Color.BLACK);
            nText.setText(c.getNombre());
            nText.setTextSize(TypedValue.COMPLEX_UNIT_PT, 8);
            nRow.addView(nText);
            Button nButton = new Button(ListActivity.this);
            Button bButton = new Button(ListActivity.this);
            nButton.setText(getString(R.string.accver));

            nButton.setText(getString(R.string.accver));
            bButton.setText(getString(R.string.accborrar));
            bButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 8);
            bButton.setTextColor(Color.BLACK);
            bButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Contacto c = (Contacto)view.getTag(R.string.contacto_g);
                    long id=0;
                    for(int a=0;a<contactos.size();a++)
                    {
                        if(contactos.get(a).getID()==id)
                        {
                            contactos.remove(a);
                            break;
                        }
                    }
                    contactos = filther;
                    tbLists.removeAllViews();
                    cargarContactos();

                }
            });

            nButton.setTextSize(TypedValue.COMPLEX_UNIT_PT, 8);
            nButton.setTextColor(Color.BLACK);
            nButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Contacto c = (Contacto)view.getTag(R.string.contacto_g);
                    Intent i = new Intent();
                    Bundle oBundle = new Bundle();
                    oBundle.putSerializable("contacto", c);
                    oBundle.putInt("index", Integer.valueOf(view.getTag(R.string.Contacto_g_index).toString()));
                    i.putExtras(oBundle);
                    setResult(RESULT_OK,i);
                    finish();
                }
            });
            nButton.setTag(R.string.contacto_g,c);
            nButton.setTag(R.string.Contacto_g_index, x);
            nRow.addView(nButton);
            bButton.setTag(R.string.contacto_g,c);
            bButton.setTag(R.string.Contacto_g_index, x);
            nRow.addView(bButton);
            tbLists.addView(nRow);
        }
    }
}
